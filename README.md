#                                   **Who am I ?**



Hello, here a fast presentation of myself. My name is Nolan, I'm a Web Dev learner in *Simplon.co* ! After one month of training, I'll say you all of what I learned: 

#### <u>HTML:</u>

After some lessons and exercises in HTML, I acquired the basics of the language. I actually know a few of tags and know how to create the structure of a Web site  !

#### <u>CSS:</u>

Few days after, we linked our first HTML competences with some CSS. As HTML, we did exercises.

Here a list of them: 

- ###### Bootstrap exercises 

- ###### Flex-box exercises 

- ###### Grid exercises



​                                           **With my actual skills, I realized the first version of my Portfolio**:  

![Imgur](https://i.imgur.com/yt570GX.jpg)

#### <u>JavaScript:</u> 

The last and actual language that I'm actually learning is JavaScript ! Some loops, functions, events  awaited me ! 

There was sometimes where I had the feeling that I did not understand anything and some better moments where I was proud of what I could do !

I actually work on a video-game created with JavaScript ! It's hard to not drown among all our functions and variables ! It's because I need to organize myself and do my code step by step to be sure that I understand everything I do !

​                                                                  <u>A screen-shot of the game actually:</u> 

![Imgur](https://i.imgur.com/XdOZ7Ap.png)

---------------------------------------------------------------------------------------------------------------------------------------------------------------



####  				Thanks for reading ! I will update with each new skill I'm going to get !		









